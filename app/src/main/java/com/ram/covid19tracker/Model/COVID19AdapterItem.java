package com.ram.covid19tracker.Model;

public class COVID19AdapterItem implements Comparable<COVID19AdapterItem>{
    private String total;
    private String death;
    private String country;
    private String newCases;
    private String flagImg;

    public COVID19AdapterItem(String total, String death, String country, String flagImg, String newCases) {
        this.total = total;
        this.country = country;
        this.death = death;
        this.flagImg = flagImg;
        this.newCases = newCases;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDeath() {
        return death;
    }

    public void setDeath(String death) {
        this.death = death;
    }

    public String getNewCases() {
        return newCases;
    }

    public void setNewCases(String newCases) {
        this.newCases = newCases;
    }

    public String getFlagImg() {
        return flagImg;
    }

    public void setFlagImg(String flagImg) {
        flagImg = flagImg;
    }

    @Override
    public int compareTo(COVID19AdapterItem o) {
        if(total.compareTo(o.total) == 0) {
            return 0;
        }
        else if(total.compareTo(o.total) < 0) {
            return 1;
        }
        else {
            return -1;
        }
    }
}
