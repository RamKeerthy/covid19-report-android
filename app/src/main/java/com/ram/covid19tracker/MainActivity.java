package com.ram.covid19tracker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;

import com.ram.covid19tracker.Adapters.COVID19Adapter;
import com.ram.covid19tracker.Model.COVID19AdapterItem;
import com.ram.covid19tracker.Model.DataVault;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements VolleyCallback{

    ArrayList<COVID19AdapterItem> covid19AdapterItems;
    COVID19Adapter covid19Adapter;
    RecyclerView recyclerView;
    AdView mAdView;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_covid);

        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);

        covid19AdapterItems = new ArrayList<COVID19AdapterItem>();

        mAdView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        String URL = "https://corona-virus-stats.herokuapp.com/api/v1/cases/countries-search?limit=200";

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Fetching Report");
        progressDialog.setMessage("Wait while we fetch the report");
        progressDialog.setCancelable(false);
        progressDialog.show();

        VolleyAPIService volleyAPIService = new VolleyAPIService();
        volleyAPIService.callback = MainActivity.this;
        volleyAPIService.volleyGet(URL, MainActivity.this);
    }

    void onReloadClicked() {
        super.onResume();

        String URL = "https://corona-virus-stats.herokuapp.com/api/v1/cases/countries-search?limit=200";

        VolleyAPIService volleyAPIService = new VolleyAPIService();
        volleyAPIService.callback = MainActivity.this;
        volleyAPIService.volleyGet(URL, MainActivity.this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.file_list_menu, menu);//Menu Resource, Menu
        MenuItem searchItem = menu.findItem(R.id.search);

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                onReloadClicked();
                return true;
            }
        });

        SearchManager searchManager = (SearchManager) MainActivity.this.getSystemService(Context.SEARCH_SERVICE);

        androidx.appcompat.widget.SearchView searchView = null;
        if (searchItem != null) {
            searchView = (androidx.appcompat.widget.SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(MainActivity.this.getComponentName()));
        }
        searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        final SearchView finalSearchView = searchView;
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                finalSearchView.clearFocus();
                onSearchQuerySubmit(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.reload:
                onReloadClicked();
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void onSearchQuerySubmit(String query) {
        String URL = "https://corona-virus-stats.herokuapp.com/api/v1/cases/countries-search?" + "search="+ query + "&limit=20";

        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setTitle("Fetching Report");
        progressDialog.setMessage("Wait while we fetch the report");
        progressDialog.setCancelable(false);
        progressDialog.show();

        VolleyAPIService volleyAPIService = new VolleyAPIService();
        volleyAPIService.callback = MainActivity.this;
        volleyAPIService.volleyGet(URL, MainActivity.this);
    }

    @Override
    public void onSuccess(String result) {
        String data = "";
        try {
            JSONObject reader = new JSONObject(result);
            data = reader.getString("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String rows = "";
        try {
            JSONObject reader = new JSONObject(data);
            rows = reader.getString("rows");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String lastUpdate;
        try {
            JSONObject reader = new JSONObject(data);
            lastUpdate = reader.getString("last_update");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        DataVault.COVID19Row[] covid19Rows = gson.fromJson(rows, DataVault.COVID19Row[].class);

        covid19AdapterItems.clear();

        for (int i = 0; i < covid19Rows.length; i++) {
            String newCases = "New cases: " + covid19Rows[i].new_cases + " New Deaths: " + covid19Rows[i].new_deaths;
            COVID19AdapterItem covid19AdapterItem = new COVID19AdapterItem(covid19Rows[i].total_cases,
                    covid19Rows[i].total_deaths,
                    covid19Rows[i].country,
                    covid19Rows[i].flag,
                    newCases);
            covid19AdapterItems.add(covid19AdapterItem);
        }

        covid19Adapter = new COVID19Adapter(R.layout.country_list_row, covid19AdapterItems);
        recyclerView = (RecyclerView) findViewById(R.id.covidrecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(covid19Adapter);

        progressDialog.dismiss();
    }
}
