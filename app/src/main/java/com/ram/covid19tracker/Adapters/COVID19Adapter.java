package com.ram.covid19tracker.Adapters;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ram.covid19tracker.Model.COVID19AdapterItem;
import com.ram.covid19tracker.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class COVID19Adapter extends RecyclerView.Adapter<COVID19Adapter.ViewHolder> {
    private static final String TAG = "PassbookAdapter";

    private int listItemLayout;
    private ArrayList<COVID19AdapterItem> COVID19AdapterItemArrayList;

    public COVID19Adapter(int layoutId, ArrayList<COVID19AdapterItem> passbookAdapterItemArrayList) {
        listItemLayout = layoutId;
        this.COVID19AdapterItemArrayList = passbookAdapterItemArrayList;
    }

    @Override
    public int getItemCount() {
        return COVID19AdapterItemArrayList == null ? 0 : COVID19AdapterItemArrayList.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.country_list_row, parent, false);
        ViewHolder myViewHolder = new ViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int listPosition) {

        TextView total = holder.total;
        TextView country = holder.country;
        TextView death = holder.death;
        TextView newCases = holder.newCases;
        ImageView flagImg = holder.flagImg;

        COVID19AdapterItem p = COVID19AdapterItemArrayList.get(listPosition);

        total.setTextColor(Color.BLACK);
        death.setTextColor(Color.RED);

        total.setText(p.getTotal());
        death.setText(p.getDeath());
        country.setText(p.getCountry());
        newCases.setText(p.getNewCases());

        Picasso.get().load(p.getFlagImg()).into(flagImg);
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView total;
        public TextView country;
        public TextView death;
        public TextView newCases;
        public ImageView flagImg;

        public ViewHolder(View itemView) {
            super(itemView);
            total = itemView.findViewById(R.id.row_total_cases);
            country = itemView.findViewById(R.id.row_country_name);
            death = itemView.findViewById(R.id.row_death_cases);
            newCases = itemView.findViewById(R.id.new_cases);
            flagImg = itemView.findViewById(R.id.row_flag);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.i(TAG, "onClick: ");
        }
    }
}